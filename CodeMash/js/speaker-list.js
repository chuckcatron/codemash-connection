Conference.speakerList = (function() {
    var viewModel = kendo.observable({
        speakerList: {},
    
        ds: new kendo.data.DataSource({
            transport: {
                read: {
                    url: Conference.configuration.getSpeakerListUrl
                }
            },
            schema: {
                data: "Speakers",
                model:{
                    id: "SpeakerURI",
                    fields:{
                        Biography: "Biography",
                        Name: "Name",
                        SpeakerURI: "SpeakerURI",
                        TwitterHandle: "TwitterHandle"
                    }
                },
                total: "Count"
            },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 25,
        })
    });
    
    function init(e) {
        console.log("init");
    	$("#speakers-list-view").kendoMobileListView({
            dataSource: viewModel.ds,
            template: $("#tmpl-speaker-list").text(),
            pullToRefresh: true,
            endlessScroll: true
        });    
    }
    return {
        initialize: init,
        viewModel: viewModel
    }
})();