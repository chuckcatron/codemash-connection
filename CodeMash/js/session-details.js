Conference.sessionDetails = (function () {

    //ViewModel to be bound to the view
    var viewModel = kendo.observable({
        Id: "",
        Title: "",
        Abstract: "",
        Difficulty: "",
        SpeakerName: "",
        Technology: "",
        URI: "",
        EventType: "",
        SessionLookupId: "",
        SpeakerURI: "",
        SpeakerLink: "",
        Start: "",
        End: "",
        Room: "",
        SecondarySpeakerName: "",
        RoomTime: ""
    });  

    //handler for show event of the view
    function show(e) {
        e.view.scroller.reset();
        viewModel = Conference.sessionList.viewModel.ds.get(e.view.params.Id);
        
        viewModel.set("SpeakerLink", "<a href='#mt-speaker-view?URI=" + viewModel.get("SpeakerURI") + "'>" + viewModel.get("SpeakerName") + "</a>");
        var dt;
        
        dt = new Date(viewModel.Start);
        
        viewModel.set("RoomTime", function(){
            $("#spanRoomTime").show();
        	var dt = new Date(viewModel.get("Start"));
            var r = viewModel.get("Room");
            if(dt.getFullYear() > 0 && r != ""){
                return kendo.toString(dt, "M/d/yyyy h:mm") + ", " + r;
            }
            if(dt.getFullYear() > 0 && r == ""){
                return kendo.toString(dt, "M/d/yyyy h:mm");
            }
            if(dt.getFullYear() <= 0 && r != ""){
                return r;
            }
            if(dt.getFullYear() <= 0 && r == ""){
                $("#spanRoomTime").hide();
                return "";
            }
        });
        kendo.bind("#session-view", viewModel);
    }

    return {
        show: show,
        viewModel: viewModel       
    }
})();