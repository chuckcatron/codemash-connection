
Conference.main = (function () {
    var application;

    function getApplication() {
        return application;
    }

    function initializeApp() {

        //initialize app
        application = new kendo.mobile.Application(document.body,
         {             
             transition: 'fade',
             loading: "",
             platform: {
                device: "ipad",       // Mobile device, can be "ipad", "iphone", "android", "fire", "blackberry", "wp", "meego"
                name: "ios",          // Mobile OS, can be "ios", "android", "blackberry", "wp", "meego"
                ios: true,            // Mobile OS name as a flag
                majorVersion: 7,      // Major OS version
                minorVersion: "0.0",  // Minor OS versions
                flatVersion: "700",   // Flat OS version for easier comparison
                appMode: false,       // Whether running in browser or in AppMode/PhoneGap/Icenium.
                tablet: "ipad"        // If a tablet - tablet name or false for a phone.
            }
             
         });

        //Display loading image on every ajax call
        $(document).ajaxStart(function () {           

            //application.showLoading calls the showLoading() method of the 
            //pane object inside the application. During the application's 
            //initial view's init method this pane object may not be initialized
            //and so application.showLoading() may throw error.To prevent this
            //we need to do a check for existence application.pane before calling
            //application.showLoading();
            if (application.pane) {
                application.showLoading();
            }            
        });

        //Hide ajax loading image on after ajax call
        $(document).ajaxStop(function () {
            
            if (application.pane) {
                application.hideLoading();
            }
        });
    }
	
     
    
    return {                
        initializeApp: initializeApp,
        getKendoApplication: getApplication,
    }
})();