Conference.sessionList = (function() {
    var viewModel = kendo.observable({
        sessionList: {},
    
        ds: new kendo.data.DataSource({
            transport: {
                read: {
                    url: Conference.configuration.getSessionListUrl,
                    data: {
                        q: function(){
                            var s = $("#searchInput").val();
                            $("#searchInput").val('');
                            return s;
                        }
                    }
                }
            },
            schema: {
                data: "Sessions",
                model:{
                    id: "Id",
                    fields:{
                        Id: { editable: false, nullable: true},
                        Title: "Title",
                        Abstract: "Abstract",
                        Difficulty: "Difficulty",
                        SpeakerName: "SpeakerName",
                        Technology: "Technology",
                        URI: "URI",
                        EventType: "EventType",
                        SessionLookupId: "SessionLookupId",
                        SpeakerURI: "SpeakerURI",
                        Start: "Start",
                        End: "End",
                        Room: "Room",
                        SecondarySpeakerName: "SecondarySpeakerName"
                    }
                },
                total: "Count"
            },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 40,
        }),
        
        doSearch: function(){
        	viewModel.ds.read();
            $("#right-drawer").data("kendoMobileDrawer").hide();
    	}
    });
    
    function init(e) {
        console.log("init");
    	$("#sessions-list-view").kendoMobileListView({
            dataSource: viewModel.ds,
            template: $("#tmpl-session-list").text(),
            pullToRefresh: true,
            endlessScroll: true
        });    
    };
        
    return {
        initialize: init,
        viewModel: viewModel
    }
})();