Conference.configuration = (function () {

    var serviceUrl = "http://codemash.chilifunfactory.com/api/";
    
    return {            
        serviceUrl: serviceUrl,
        getSessionListUrl: serviceUrl + "Sessions/GetSessionList/",
		getSpeakerListUrl: serviceUrl + "Speakers/GetSpeakerList/",
        getSpeakerByURIUrl: serviceUrl + "Speakers/GetSpeakerByURI/"
    }
})();