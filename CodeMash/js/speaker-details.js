Conference.speakerDetails = (function () {

    //ViewModel to be bound to the view
    var viewModel = kendo.observable({
        Id: "",
        Name: "",
        SpeakerURI: "",
        Biography: "",
        TwitterHandle: ""
    });  

    //handler for show event of the view
    function show(e) {
        e.view.scroller.reset();
        $("#spanTwitterHandle").hide();
        var serviceOptions = {
            url: Conference.configuration
                .getSpeakerByURIUrl + '?URI=' + e.view.params.URI,
            requestType: "GET",
            dataType: "JSON",
            callBack: callBack
        };
        Conference.dataAccess.callService(serviceOptions);
                kendo.bind("#mt-session-view", viewModel);
    }
    
    function callBack(result) {
        if (result.success === true) {
            viewModel.set("Name", result.data.Name);
            viewModel.set("Biography", result.data.Biography);
            viewModel.set("TwitterHandle", result.data.TwitterHandle);
            
            var th = viewModel.get("TwitterHandle");
            if(th != ""){
                th = th.replace("@", "");
                viewModel.set("TwitterHandle", "<br/>Twitter: @" + th);
            	$("#spanTwitterHandle").show();
            } else {
                $("#spanTwitterHandle").hide();
            }
        }
    }

    return {
        show: show,
        viewModel: viewModel       
    }
})();